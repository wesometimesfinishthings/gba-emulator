
;MAY NEED TO USE THIS. HOLD.


.code

; USAGE: shifter_ror( rotate_imm, immediate )
shifter_ror PROC

; RCX holds first value never4get asm loser teuvin
ror edx, cl ; rotate_imm is only 4 bits
mov eax, edx
ret

shifter_ror ENDP

; USAGE: arithmetic_shr( shiftamount, val )
arithmetic_shr PROC

sar edx, cl
mov eax, edx
ret

arithmetic_shr ENDP

; USAGE: arithmetic_shl( shiftamount, val )
arithmetic_shl PROC

sal edx, cl
mov eax, edx
ret

arithmetic_shl ENDP

END