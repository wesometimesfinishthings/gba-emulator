#define NOMINMAX // fuck off windows
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <cstdint>
#include <limits>
#include <tuple>

/*
References:
http://www.atmel.com/images/ddi0029g_7tdmi_r3_trm.pdf
http://profile.iiita.ac.in/bibhas.ghoshal/lecture_slides_coa/arm_inst.pdf
https://www.scss.tcd.ie/~waldroj/3d1/arm_arm.pdf
*/


namespace ARM {

#pragma region Forward Dec
	namespace Internals {
		struct Regs;
		struct ARMInstruction;
		enum class DataOperandType;
	}
#pragma endregion

	class ARMProcessor {
		bool bArmState; // arm state or thumb state
						/*
						In ARM state:
						bits 0->1 of PC are undefined, 2->32 are valid
						Access to:
						16 GP registers + 1/2 state registers
						In Thumb state:
						bit 0 of pc is undefined.
						Access to:
						r0->r7
						PC
						SP
						LR
						CPSR (Current Program Status Register)
						Mappings to ARM:
						r0-r7 are identical.
						CPSR + SPSR identical
						SP maps to r13
						LR maps to r14
						PC maps to r15
						*/

		bool ConditionMet( Internals::ARMInstruction * pInstruction );
		// Negative && Zero -- CALL AFTER EVERY CPU INSTRUCTION EXECUTES!
		bool UpdateSimpleFlags( Internals::ARMInstruction * pInstruction );
		std::tuple<Internals::DataOperandType, uint32_t, uint32_t> DecodeDataProcessing( Internals::ARMInstruction * pInstruction );
		bool GetBit( uint32_t value, int place );
#pragma region ARM Instructions
#pragma endregion

	public:
		Internals::Regs* registers;

		void armADC( Internals::ARMInstruction& instr );
		void armADD( Internals::ARMInstruction& instr );
		void armAND( Internals::ARMInstruction& instr );
	};

	namespace Internals {

		struct Regs {
			union {
				uint32_t r[ 31 ]; // 31 general purpose registers.
				class {
					uint32_t pad[ 8 ];

				public:
					/*R8->R12 have 2 versions ( all / FIQ )*/
					uint32_t r8_usr;		// 8
					uint32_t r9_usr;		// 9
					uint32_t r10_usr;		// 10
					uint32_t r11_usr;		// 11
					uint32_t r12_usr;		// 12

					/*	These are special named registers.	*/
					uint32_t stack;			// 13	( r13 usr )
					/*
						R14 ( link ) gets copy of r15 ( pc ) when a branch with link instruction is executed
					*/
					uint32_t link;			// 14	( r14 usr )
					uint32_t pc;			// 15

					/*FIQ Registers*/

					uint32_t r8_fiq;		// 16
					uint32_t r9_fiq;		// 17
					uint32_t r10_fiq;		// 18
					uint32_t r11_fiq;		// 19
					uint32_t r12_fiq;		// 20

					/*Registers R13 & R14 have 6 banked registers each.*/

					// Supervisor Mode
					uint32_t r13_svc;		// 21
					uint32_t r14_svc;		// 22

					// Abort mode
					uint32_t r13_abt;		// 23
					uint32_t r14_abt;		// 24

					// Undefined Instruction mode
					uint32_t r13_und;		// 25
					uint32_t r14_und;		// 26

					// Interrupt Request mode
					uint32_t r13_irq;		// 27
					uint32_t r14_irq;		// 28

					// Fast Interrupt Request mode
					uint32_t r13_fiq;		// 29
					uint32_t r14_fiq;		// 30

				} named;
			};

			// These flags relate to the CPSR
			//enum StatusFlags : uint8_t {
			//	mode, // 0->4
			//	state = 5,
			//	fiq_disable,
			//	irq_disable,
			//	overflow = 27,
			//	cbextend,		// carry/borrow/extend
			//	zero,
			//	neg
			//};


			struct statusregister {
				uint32_t full;
				uint32_t mode : 4;				// 0->4
				uint32_t thumb : 1;				// 5 ( Set && !jazelle == thumb mode ) : Set && jazelle == RESERVED
				uint32_t fiq_disable : 1;		// 6
				uint32_t irq_disable : 1;		// 7
				uint32_t abort_disable : 1;		// 8
				uint32_t endian_big : 1;		// 9 ( Set == Big endian mode )
				uint32_t reserved : 6;			// 10->15
				uint32_t geq : 4;				// 16->19 ( Greather than or equal )
				uint32_t reserved2 : 4;			// 20 -> 23
				uint32_t jazelle : 1;			// 24 ( Set && !thumb == Jazelle mode ) Set && thumb == RESERVED
				uint32_t reserved3 : 2;			// 25->26
				uint32_t q : 1;					// 27 ( Overflow and/or saturation in DSP instructions. idk. its weird. )
				/*
					28
					Set when:
						+/- : Signed overflow occured.
						else: Usually unchanged unless otherwise specified.
				*/
				uint32_t overflow : 1;
				/*
					29 Set when: In +, if addition produces a carry.
					.In -, if subtraction requires a borrow, otherwise if shift,
					C==last bit shifted out of value by shifter.
				*/
				uint32_t carry : 1;

				uint32_t zero : 1;				// 30 Set to 1 if the result of the instruction is 0, else 0
				uint32_t negative : 1;			// 31 Set to bit 31 of the result of the instruction. 
			};

			statusregister cpsr;				// spsr_usr ?
			statusregister spspr_irq;
			statusregister spspr_fiq;
			statusregister spspr_svc;
			statusregister spspr_abt;
			statusregister spspr_und;
			//statusregister spspr_sys;			// system.
		};

		enum Conditions : uint8_t {
			EQ,				// Equal ( Z set )
			NE,				// Guess. ( Z !set )
			CS,				// Carry set, Also called HS. Carry set/unsigned higher or same ( C set )
			CC,				// Carry clear, called LO. Carry !set/unsigned lower ( C !set )
			MI,				// Minus. Also called negative ( N set )
			PL,				// Positive ( N !set )
			VS,				// Overflow ( V set )
			VC,				// !Overflow ( V !set )
			HI,				// Unsigned higher ( C set && Z !set )
			LS,				// Unsigned lower or same ( C !set && Z set )
			GE,				// Signed greater than or equal ( N == V )
			LT,				// Signed less than ( N != V )
			GT,				// Signed greater than ( Z == 0 && N == V )
			LE,				// Signed less than or equal ( Z && N != V )
			AL,				// Always.
			UNPREDICTABLE	// BAD SHIT.
		};

		enum class DataOperandType {
			imm,
			reg,

			shlimm, // logical shift left imm
			shlreg, // logical shift left reg

			shrimm,	// logical shift right imm
			shrreg,	// logical shift right reg

			asrimm,	// arithmetic shift right imm
			asrreg, // arithmetic shift right reg

			rorimm,	// rotate right imm
			rorreg,	// rotate right reg

			rorext, // rotate right w/ extend

			INVALID
		};

		struct ARMInstruction { // too fucking lazy to finish this <1st Arithmetic Source>.
			union {
				uint32_t instr;						// 32 bits

				union {
					struct {
						union {
							struct {
								union {
									struct {
										uint32_t rm : 4;			// bits 0->3 ( 2nd Arithmetic Source )
										uint32_t mustbezero : 3;	// bit 4->6
										uint32_t shift_amount : 5;	// bits 7 -> 11
									} shlimm;

									struct {
										uint32_t rm : 4;			// bits 0->3 ( 2nd Arithmetic Source )
										uint32_t mustbeone : 1;		// bit 4
										uint32_t mustbezero : 3;	// bits 5->7
										uint32_t rs : 4;			// bits 8 -> 11	
									} shlreg;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbetwo : 3;		// bits 4->6
										uint32_t shift_amount : 5;	// bits 7->11
									} shrimm;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbethree : 4;	// bits 4->7
										uint32_t rs : 4;			// bits 8->11
									} shrreg;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbefour : 3;	// bits 4->6
										uint32_t shift_amount : 5;	// bits 7->11
									} asrimm;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbefive : 4;	// bits 4->7
										uint32_t rs : 4;			// bits 8->11
									} asrreg;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbesix : 3;		// bits 4->6
										uint32_t shift_amount : 5;	// bits 7->11
									} rorimm;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbeseven : 4;	// bits 4->7
										uint32_t rs : 4;			// bits 8->11
									} rorreg;

									struct {
										uint32_t rm : 4;			// bits 0->4
										uint32_t mustbesix : 8;		// bits 4->11
									} rorext;
								};
							} shifts;

							struct {
								uint32_t shift : 8;			// bits 0->7
								uint32_t rotate : 4;		// bits 8->11
							} imm; // no shift

							struct {
								uint32_t rm : 4;			// bits 0->3 ( 2nd Arithmetic Source )
								uint32_t mustbezero : 8;	// bits 4->11
							} reg;
						};
					} data;

					struct {
						union {
							struct {
								uint32_t offset : 12;	// bits 0->11
							} imm;

							struct 								{
								uint32_t rm : 4;		// bits 0->3
								uint32_t mustbezero : 8;// bits 4->11
							} reg;

							struct {
								uint32_t rm : 4;		// bits 0->3
								uint32_t zero : 1;		// bit 4
								uint32_t shift : 2;		// bit 5->6
								uint32_t shift_imm : 4;	// bits 7->11
							} scaled_reg;
						};

						uint32_t rd : 4;				// bits 12->15
						uint32_t rn	: 4;				// bits 16->19
						uint32_t l : 1;					// bit 20 ( Distinguish between load ( l ) and store ( !l ) )
						/*
						Bit 21
							Has two meanings:
							P == 0:
								If W == 0, the instruction is LDR, LDRB, STR or STRB and a normal memory access
							is performed.
								If W == 1, the instruction is LDRBT, LDRT, STRBT or STRT and an
							unprivileged (User mode) memory access is performed.
							P == 1:
								If W == 0, the base register is not updated (offset addressing).
								If W == 1, the calculated memory address is written back to the base register (pre-indexed
							addressing).
						*/
						uint32_t w : 1;
						uint32_t b : 1;					// bit 22 ( unsigned byte ( b ) && word ( !b ) access )
						uint32_t u : 1;					// bit 23 ( Whether offset is added or subtracted from base ( !u ) )
						uint32_t p : 1;					// bit 24 ( post indexed addressing )
						uint32_t reg : 1;				// bit 25 ( seems to be set when using register )
						uint32_t mustbeone : 2;			// bits 26->27
						uint32_t condition : 4;			// bits 28->31



					} lsword; // load & store word/unsigned byte

				};

				struct {
					uint32_t rd : 4;					// bits 12->15 ( Destination Register )
					uint32_t rn : 4;					// bits 16->19 ( 1st Arithmetic Source )
					uint32_t s : 1;						// bit 20 ( Indicates that instruction updates condition codes )
					uint32_t opcode : 4;				// bits 21->24
					uint32_t i : 1;						// bit 25 ( Distinguish between immediate and register forms of instructions )
					uint32_t reserved2 : 2;				// bits 26->27
					uint32_t condition : 4;				// bits 28->31 ( If unsatisfied, check for interrupt & aborts, otherwise NOP )
				};

				struct {
					uint32_t immed : 24;				// bits 0->23 ( fuck i hate arm )
					uint32_t link : 1;					// bit 24
					uint32_t discrim : 3;				// bits 25->27 discriminate between types.
					uint32_t condition : 4;				// bits 28->31
				} branch;

				struct {
					uint32_t immed : 4;					// bits 0->3
					uint32_t mustbeseven : 4;			// bits 4->7
					uint32_t immed2 : 12;				// bits 8->19
					uint32_t mustbetwelve : 8;			// bits 20->27
					uint32_t mustbefourteen : 4;		// bits 28->31
				} breakpoint;
			};
		};

		struct InstructionHandler {
			uint32_t opcode;
			using func = decltype( &ARMProcessor::armADC );
			func effector;
		};
	}

	static Internals::InstructionHandler armInstructions[ ] =
	{
		{0b0101, &ARMProcessor::armADC},
		{0b0100, &ARMProcessor::armADD},

	};
}

//Ease of use.
#define REG(x) this->registers->r[x]
